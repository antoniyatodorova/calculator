# Calculator

<img src="calculator.png">

## Description

This project is a simple calculator built in C# using Windows Forms in the Visual Studio development environment. The calculator provides basic mathematical operations such as addition, subtraction, multiplication, and division, along with additional functions.

## Installation

To use this calculator:

1. Clone this GitLab repository to your computer or download the code as an archive.
2. Open the project in Visual Studio.
3. Run the application by clicking the "Start" button in Visual Studio or using the executable file provided in this repository.

## How to Use the Calculator

The calculator offers the following features:

- Addition
- Subtraction
- Multiplication
- Division
- Percentage
- Change sign of a number
- Add a decimal point
- Clear Entry (CE)
- Clear All (C)

To perform a calculation, follow these steps:

1. Press the respective digit buttons to enter numbers.
2. Select a mathematical operation (+, -, *, /, %) by clicking the corresponding button.
3. Enter the second number.
4. Press "=" to see the result of the calculation.




[@antoniyatodorova] - Developer

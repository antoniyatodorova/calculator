﻿using System;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Calculator : Form
    {
        string sign;
        double result;
        bool startNewNumber = true;

        public Calculator()
        {
            InitializeComponent();
            textBox1.Text = "0";
            result = 0;
        }

        private void NumberButton_Click(string number)
        {
            if (startNewNumber)
            {
                textBox1.Text = number;
                startNewNumber = false;
            }
            else
            {
                textBox1.Text += number;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NumberButton_Click("1");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            NumberButton_Click("2");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            NumberButton_Click("3");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            NumberButton_Click("4");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            NumberButton_Click("5");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            NumberButton_Click("6");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            NumberButton_Click("7");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            NumberButton_Click("8");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            NumberButton_Click("9");
        }

        private void button0_Click(object sender, EventArgs e)
        {
            NumberButton_Click("0");
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            PerformOperation();
            sign = "+";
        }

        private void buttonSubtract_Click(object sender, EventArgs e)
        {
            PerformOperation();
            sign = "-";
        }

        private void buttonMultiply_Click(object sender, EventArgs e)
        {
            PerformOperation();
            sign = "*";
        }

        private void buttonDivide_Click(object sender, EventArgs e)
        {
            PerformOperation();
            sign = "/";
        }

        private void buttonPercentage_Click(object sender, EventArgs e)
        {
            PerformOperation();
            sign = "%";
        }

        private void buttonEqual_Click(object sender, EventArgs e)
        {
            PerformOperation();
            sign = string.Empty;
        }

        private void PerformOperation()
        {
            double currentValue = double.Parse(textBox1.Text);

            switch (sign)
            {
                case "+":
                    result += currentValue;
                    break;
                case "-":
                    result -= currentValue;
                    break;
                case "*":
                    result *= currentValue;
                    break;
                case "/":
                    if (currentValue != 0)
                    {
                        result /= currentValue;
                    }
                    else
                    {
                        textBox1.Text = "Error: Division by zero!";
                        result = 0;
                    }
                    break;
                case "%":
                    result %= currentValue;
                    break;
                default:
                    result = currentValue;
                    break;
            }

            textBox1.Text = result.ToString();
            startNewNumber = true;
        }

        private void buttonChangeToOpposite_Click(object sender, EventArgs e)
        {
            double currentValue = double.Parse(textBox1.Text);
            currentValue = -currentValue;
            textBox1.Text = currentValue.ToString();
        }

        private void buttonComma_Click(object sender, EventArgs e)
        {
            if (!textBox1.Text.Contains(","))
            {
                textBox1.Text += ",";
            }
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            textBox1.Text = "0";
            result = 0;
            sign = string.Empty;
            startNewNumber = true;
        }

        private void buttonCE_Click(object sender, EventArgs e)
        {
            textBox1.Text = "0";
            startNewNumber = true;
        }
    }
}
